dav1d_wasm_release_build_path="build/dav1d-wasm-release"
dav1d_wasm_debug_build_path="build/dav1d-wasm-debug"
dav1d_native_release_build_path="build/dav1d-native-release"
dav1d_wasm_release_library_path="build/dav1d-wasm-release/src/libdav1d.a"
dav1d_wasm_debug_library_path="build/dav1d-wasm-debug/src/libdav1d.a"

all: dav1d.wasm

$(dav1d_wasm_release_library_path):
	meson dav1d $(dav1d_wasm_release_build_path) \
		--prefix="$(CURDIR)/out/wasm/release" \
		--cross-file=cross_file_release.txt \
		--buildtype=release \
		--default-library=static \
		-Dbitdepths="['8']" \
		-Denable_asm=false \
		-Denable_tools=false \
		-Denable_tests=false \
		-Dlogging=false \
		-Dfake_atomics=true \
	&& ninja -C $(dav1d_wasm_release_build_path) install

$(dav1d_wasm_debug_library_path):
	meson dav1d $(dav1d_wasm_debug_build_path) \
		--prefix="$(CURDIR)/out/wasm/debug" \
		--libdir=lib \
		--cross-file=cross_file_debug.txt \
		--default-library=static \
		--buildtype=debug \
		-Dbitdepths="['8']" \
		-Denable_asm=false \
		-Denable_tools=false \
		-Denable_tests=false \
		-Dlogging=false \
		-Dfake_atomics=true \
	&& ninja -v -C $(dav1d_wasm_debug_build_path) install

out/native/release/tools/dav1d:
	meson dav1d $(dav1d_native_release_build_path) \
		--prefix="$(CURDIR)/out/wasm/release" \
		--cross-file=cross_file_release.txt \
		--buildtype=release \
		--default-library=static \
		-Dbitdepths="['8']" \
		-Denable_asm=false \
		-Denable_tools=false \
		-Denable_tests=false \
		-Dlogging=false \
		-Dfake_atomics=true \
	&& ninja -C $(dav1d_native_release_build_path) install

dav1d.wasm: $(dav1d_wasm_release_library_path) dav1d.c
	emcc $^ -DNDEBUG -O3 --profiling  --llvm-lto 3 -Iout/wasm/release/include -Wl,--no-check-features --no-entry -o $@ \
		-s TOTAL_MEMORY=256mb -s TOTAL_STACK=64mb

dav1d.debug.wasm: $(dav1d_wasm_debug_library_path) dav1d.c
	EMCC_DEBUG=2 emcc $^ -g4 -O0 --profiling  -Iout/wasm/debug/include -Wl,--no-check-features --no-entry -s ASSERTIONS=1 -s SAFE_HEAP=1 --source-map-base http:\/\/localhost:8000\/ -gseparate-dwarf=dav1d.debug.dwarf.wasm -o $@ \
		-s TOTAL_MEMORY=256mb -s TOTAL_STACK=64mb

.PHONY: test
test: dav1d.c
	$(CC) $^ $(CFLAGS) -O2 -Wall -o $@ \
		-I../tmp/dav1d/dist/include -L../tmp/dav1d/dist/lib \
		-ldav1d -lpthread

test-debug: dav1d.c
	$(CC) $^ $(CFLAGS) -O0 -g -Wall -o $@ \
		-I../tmp/dav1d/dist/include -L../tmp/dav1d/dist/lib \
		-ldav1d -lpthread

test-native: test
	./test

test-valgrind: CFLAGS = -DDJS_VALGRIND
test-valgrind: test
	valgrind ./test

test-node: dav1d.wasm
	node --experimental-modules --preserve-symlinks test.mjs

clean: clean-build clean-wasm clean-test
clean-build:
	rm -rf build
clean-wasm:
	rm -f dav1d.wasm
clean-test:
	rm -f test
