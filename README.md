# dav1d.js [![npm](https://img.shields.io/npm/v/dav1d.js.svg)](https://www.npmjs.com/package/dav1d.js)

AV1 decoder for runtimes without AV1 support.

Powered by fast [dav1d](https://code.videolan.org/videolan/dav1d) library
ported to WebAssembly.

## How to Build

+ Clone the repository
+ Make sure you have latest Meson and emscripten
  + Emscripten Install Instructions: [Homepage](https://emscripten.org/docs/getting_started/downloads.html)
  + Meson Install Instructions: [Meson](https://mesonbuild.com/Quick-guide.html)
+ Fetch the submodule dav1d
  + `git submodule update --init --recursive`
  > Note: This is having dav1d 0.8.2
+ Apply Patch [`0001-Disable-thread-in-meson.patch`](0001-Introduce-fake_atomics-to-fake-for-WASM.patch)
+ `make`: This will build release version by defaullt

## License

dav1d is licensed under [BSD](https://code.videolan.org/videolan/dav1d/blob/master/COPYING).  
dav1d.js wrapper code is licensed under [CC0](COPYING).
